import CSdl2

public
enum WindowFullScreenFlags: UInt32 {

    case fullScreen = 0x00000001

    case fullScreenDesktop = 0x00001001

    case windowed = 0x00000000

}