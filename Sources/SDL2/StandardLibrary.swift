import CSdl2

extension Bool {
    var sdl: SDL_bool {
        switch self {
        case true:
            return SDL_TRUE
        case false:
            return SDL_FALSE
        }
    }
}

extension Int {
    var sdl: Int32 {
        return Int32(self)
    }
}
