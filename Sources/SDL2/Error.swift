import CSdl2

public struct SDLError: Error {
    public let message: String

    init() {
        message = String(cString: SDL_GetError())
    }
}