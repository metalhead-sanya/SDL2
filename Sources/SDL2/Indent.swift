public
struct Indent {
    let top: Int
    let left: Int
    let bottom: Int
    let right: Int

    public
    init(top: Int, left: Int, bottom: Int, right: Int) {
        self.top = top
        self.left = left
        self.bottom = bottom
        self.right = right
    }

    init(top: Int32, left: Int32, bottom: Int32, right: Int32) {
        self.top = Int(top)
        self.left = Int(left)
        self.bottom = Int(bottom)
        self.right = Int(right)
    }
}