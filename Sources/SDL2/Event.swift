import CSdl2

public enum Event {

	case quit
	case unknown

	public static func poll() -> Event? {
		var n_Event = SDL_Event()
		guard SDL_PollEvent(&n_Event) != 0 else {
			return nil
		}

		switch n_Event.type {
		case SDL_QUIT.rawValue:
			return .quit
		default:
			return .unknown
		}
	}
}
